import React, { useState } from 'react';
import './App.css';

function App() {
  const [display, setDisplay] = useState('0');
  const [operand1, setOperand1] = useState('');
  const [operator, setOperator] = useState('');
  const [waitingForOperand2, setWaitingForOperand2] = useState(false);

  const handleDigit = (digit) => {
    if (display === '0' || waitingForOperand2) {
      setDisplay(digit);
      setWaitingForOperand2(false);
    } else {
      setDisplay(display + digit);
    }
  };

  const handleOperator = (op) => {
    if (!waitingForOperand2) {
      setOperand1(display);
      setOperator(op);
      setWaitingForOperand2(true);
    }
  };

  const handleDecimal = () => {
    if (!display.includes('.')) {
      setDisplay(display + '.');
    }
  };

  const handleClear = () => {
    setDisplay('0');
    setOperand1('');
    setOperator('');
    setWaitingForOperand2(false);
  };

  const handleEqual = () => {
    if (operator && !waitingForOperand2) {
      const operand2 = display;
      const result = operate(parseFloat(operand1), parseFloat(operand2), operator);
      setDisplay(result.toString());
      setOperand1(result.toString());
      setOperator('');
      setWaitingForOperand2(true);
    }
  };

  const operate = (a, b, op) => {
    switch (op) {
      case '+':
        return a + b;
      case '-':
        return a - b;
      case '*':
        return a * b;
      case '/':
        return b === 0 ? 'Error' : a / b;
      default:
        return 'Error';
    }
  };

  return (
    <div className="calculator">
      <div className="display">{display}</div>
      <div className="buttons">
        <button onClick={() => handleDigit('7')}>7</button>
        <button onClick={() => handleDigit('8')}>8</button>
        <button onClick={() => handleDigit('9')}>9</button>
        <button onClick={() => handleOperator('/')}>÷</button>
        <button onClick={() => handleDigit('4')}>4</button>
        <button onClick={() => handleDigit('5')}>5</button>
        <button onClick={() => handleDigit('6')}>6</button>
        <button onClick={() => handleOperator('*')}>×</button>
        <button onClick={() => handleDigit('1')}>1</button>
        <button onClick={() => handleDigit('2')}>2</button>
        <button onClick={() => handleDigit('3')}>3</button>
        <button onClick={() => handleOperator('-')}>-</button>
        <button onClick={handleDecimal}>.</button>
        <button onClick={() => handleDigit('0')}>0</button>
        <button onClick={handleEqual}>=</button>
        <button onClick={() => handleOperator('+')}>+</button>
        <button onClick={handleClear}>AC</button>
      </div>
    </div>
  );
}

export default App;
